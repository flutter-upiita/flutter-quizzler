class Question {
  String questionText;
  bool questionAnswer;

  //Constructor
  Question({String q, bool a}) {
    questionAnswer = a;
    questionText = q;
  }
}
